#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import sys
import math
from fastecdsa.curve import Curve
from fastecdsa.point import Point
import random
import optparse

CNT_KAT = 10

def fe2hex(curve, x):
    bytes = math.ceil(curve.p.bit_length() / 8)
    s = '%x' % x
    while len(s) != 2*bytes:
        s = '0%s' % s
    return s

def gen_fe_inv(curve):
    # TODO BBB zero is somehow special ...
    #elems = [0, 1, 2, curve.p - 2, curve.p - 1]
    elems = [1, 2, curve.p - 2, curve.p - 1]
    for i in range(CNT_KAT):
        elems.append(random.randint(3, curve.p - 3))
    for e in elems:
        einv = pow(e, curve.p - 2, curve.p)
        print('test_expect_success "%s field inversion" "$TEST_HARNESS -i %s | grep ^%s#"' % (curve.name, fe2hex(curve, e), fe2hex(curve, einv)))

def gen_stress(curve):
    assert(curve.stress != None)
    assert(curve.stress.startswith('0x04'))
    p = curve.stress[4:]
    assert(len(p) % 2 == 0)
    px = int(p[:len(p)//2],16)
    py = int(p[len(p)//2:],16)
    print('test_expect_success "%s stress test" "$TEST_HARNESS -t | grep ^%s#%s#"' % (curve.name, fe2hex(curve, px), fe2hex(curve, py)))

def gen_stress_g(curve):
    assert(curve.stress_g != None)
    assert(curve.stress_g.startswith('0x04'))
    p = curve.stress_g[4:]
    assert(len(p) % 2 == 0)
    px = int(p[:len(p)//2],16)
    py = int(p[len(p)//2:],16)
    print('test_expect_success "%s stress test base point" "$TEST_HARNESS -T | grep ^%s#%s#"' % (curve.name, fe2hex(curve, px), fe2hex(curve, py)))

def gen_scmul_two(curve):
    bytes = math.ceil(curve.p.bit_length() / 8)
    elems = []
    # hardcode some corner cases
    for i in range(1, 34, 1):
        elems.append((i, i))
        elems.append((curve.q + i, curve.q + i))
        elems.append((curve.q - i, curve.q - i))
        elems.append((((1 << (8 * bytes)) - i), ((1 << (8 * bytes)) - i)))

    # pick some random ones too
    for i in range(CNT_KAT):
        a = random.randint(1, curve.q)
        b = random.randint(1, curve.q)
        elems.append((a, b))

    # run the kats
    for a, b in elems:
        k = random.randint(1, curve.q)
        P = k * (curve.G)
        Q = a * (curve.G) + b * P
        print('test_expect_success "%s simultaneous scalar multiplication" "$TEST_HARNESS -2 %s %s %s %s | grep ^%s#%s#"' % (curve.name, fe2hex(curve, a), fe2hex(curve, b), fe2hex(curve, P.x), fe2hex(curve, P.y), fe2hex(curve, Q.x), fe2hex(curve, Q.y)))

    # inf tests
    # first component
    a = 0
    Q = b * P
    print('test_expect_success "%s simultaneous scalar multiplication" "$TEST_HARNESS -2 %s %s %s %s | grep ^%s#%s#"' % (curve.name, fe2hex(curve, a), fe2hex(curve, b), fe2hex(curve, P.x), fe2hex(curve, P.y), fe2hex(curve, Q.x), fe2hex(curve, Q.y)))
    a = curve.q
    print('test_expect_success "%s simultaneous scalar multiplication" "$TEST_HARNESS -2 %s %s %s %s | grep ^%s#%s#"' % (curve.name, fe2hex(curve, a), fe2hex(curve, b), fe2hex(curve, P.x), fe2hex(curve, P.y), fe2hex(curve, Q.x), fe2hex(curve, Q.y)))
    # second component
    a = b
    b = 0
    Q = a * (curve.G)
    print('test_expect_success "%s simultaneous scalar multiplication" "$TEST_HARNESS -2 %s %s %s %s | grep ^%s#%s#"' % (curve.name, fe2hex(curve, a), fe2hex(curve, b), fe2hex(curve, P.x), fe2hex(curve, P.y), fe2hex(curve, Q.x), fe2hex(curve, Q.y)))
    b = curve.q
    print('test_expect_success "%s simultaneous scalar multiplication" "$TEST_HARNESS -2 %s %s %s %s | grep ^%s#%s#"' % (curve.name, fe2hex(curve, a), fe2hex(curve, b), fe2hex(curve, P.x), fe2hex(curve, P.y), fe2hex(curve, Q.x), fe2hex(curve, Q.y)))
    # both components
    print('test_expect_success "%s simultaneous scalar multiplication" "$TEST_HARNESS -2 %s %s %s %s | grep ^%s#%s#"' % (curve.name, fe2hex(curve, 0), fe2hex(curve, 0), fe2hex(curve, P.x), fe2hex(curve, P.y), fe2hex(curve, 0), fe2hex(curve, 0)))
    print('test_expect_success "%s simultaneous scalar multiplication" "$TEST_HARNESS -2 %s %s %s %s | grep ^%s#%s#"' % (curve.name, fe2hex(curve, curve.q), fe2hex(curve, curve.q), fe2hex(curve, P.x), fe2hex(curve, P.y), fe2hex(curve, 0), fe2hex(curve, 0)))

def gen_scmul(curve, is_g=True):
    bytes = math.ceil(curve.p.bit_length() / 8)
    elems = [0, curve.q]
    # hardcode some corner cases
    for i in range(1, 34, 1):
        elems.append(i)
        elems.append(curve.q + i)
        elems.append(curve.q - i)
        elems.append((1 << (8 * bytes)) - i)

    # pick some random ones too
    for i in range(CNT_KAT):
        elems.append(random.randint(2, curve.q - 2))

    P = curve.G
    if not is_g:
        # if it's not G, pick a random point
        k = random.randint(2, curve.q - 2)
        P = k*P

    # run the kats
    for e in elems:
        try:
            # some curves have cofactor killing / clearing
            Q = (curve.h * e) * P if not is_g and curve.name in ('id_tc26_gost_3410_2012_256_paramSetA', 'id_tc26_gost_3410_2012_512_paramSetC') else e * P
            x = Q.x
            y = Q.y
        except ValueError:
            x = 0
            y = 0
        if Q == Point.IDENTITY_ELEMENT:
            x = 0
            y = 0
        if is_g:
            # fixed point scmul
            print('test_expect_success "%s fixed scalar multiplication" "$TEST_HARNESS -S %s | grep ^%s#%s#"' % (curve.name, fe2hex(curve, e), fe2hex(curve, x), fe2hex(curve, y)))
        else:
            # variable point scmul
            print('test_expect_success "%s variable scalar multiplication" "$TEST_HARNESS -s %s %s %s | grep ^%s#%s#"' % (curve.name, fe2hex(curve, e), fe2hex(curve, P.x), fe2hex(curve, P.y), fe2hex(curve, x), fe2hex(curve, y)))

if __name__ == "__main__":

    name = None
    parser = optparse.OptionParser()
    parser.add_option("--curve", action="store", dest="name")
    parser.add_option("-i", action="store_true", dest="do_fe_inv", default=False)
    parser.add_option("-s", action="store_true", dest="do_scmul", default=False)
    parser.add_option("-S", action="store_true", dest="do_scmul_g", default=False)
    parser.add_option("-2", action="store_true", dest="do_scmul_two", default=False)
    parser.add_option("-t", action="store_true", dest="do_stress", default=False)
    parser.add_option("-T", action="store_true", dest="do_stress_g", default=False)
    options, args = parser.parse_args()

    # fetch the curve parameters
    try:
        with open('ecp/%s/curve.json' % (options.name)) as fp:
            params = json.load(fp)
            assert(params['name'] == options.name)
    except:
        print('usage: python3 %s --curve <curve_name>' % (sys.argv[0]))
        sys.exit(1)

    # fix the prng seed
    random.seed(params['name'])

    # setup the curve; point starts with 0x04 to indicate uncompressed
    g = params['g'][4:]
    assert(len(g) % 2 == 0)
    gx = int(g[:len(g)//2],16)
    gy = int(g[len(g)//2:],16)

    # we now have a proper curve object with all the parameters and inherited ECC methods
    curve = Curve(name=params['name'], p=int(params['p'],16), a=int(params['a'],16), b=int(params['b'],16), q=int(params['n'],16), gx=gx, gy=gy, oid=None)
    curve.h = int(params['h'], 16)

    # optionally add stress test parameters
    if 'stress' in params: curve.stress = params['stress']
    else: curve.stress = None
    if 'stress_g' in params: curve.stress_g = params['stress_g']
    else: curve.stress_g = None

    # sharness header
    print("""#!/bin/bash
test_description="%s tests"
. $SHARNESS_TEST_SRCDIR/sharness.sh""" % curve.name)

    if options.do_fe_inv: gen_fe_inv(curve)
    if options.do_scmul: gen_scmul(curve, is_g=False)
    if options.do_scmul_g: gen_scmul(curve, is_g=True)
    if options.do_scmul_two: gen_scmul_two(curve)
    if options.do_stress: gen_stress(curve)
    if options.do_stress_g: gen_stress_g(curve)
    # TODO BBB add other test functions

    # sharness footer
    print("test_done")
