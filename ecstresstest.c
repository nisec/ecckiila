#include <openssl/ec.h>
#include <assert.h>

#define STRESS_ITS (1 << 16)

int ec_stress(const EC_GROUP *group, int is_g) {
    BN_CTX *ctx = NULL;
    BIGNUM *scalar = NULL, *x = NULL, *y = NULL;
    EC_POINT *P = NULL;
    char *p = NULL;
    int i;

    assert((ctx = BN_CTX_new()) != NULL);
    BN_CTX_start(ctx);
    x = BN_CTX_get(ctx);
    y = BN_CTX_get(ctx);
    assert((scalar = BN_CTX_get(ctx)) != NULL);
    assert((P = EC_POINT_dup(EC_GROUP_get0_generator(group), group)) != NULL);

    for (i = 0; i < STRESS_ITS; i++) {
        assert(EC_POINT_get_affine_coordinates(group, P, x, y, ctx) == 1);
        assert(BN_GF2m_add(scalar, x, y) == 1);
        if (is_g)
            assert(EC_POINT_mul(group, P, scalar, NULL, NULL, ctx) == 1);
        else
            assert(EC_POINT_mul(group, P, NULL, P, scalar, ctx) == 1);
    }

    assert((p = EC_POINT_point2hex(group, P, POINT_CONVERSION_UNCOMPRESSED, ctx)) != NULL);
    printf("\"%s\": \"0x%s\"", (is_g) ? "stress_g" : "stress", p);
    free(p);
    EC_POINT_free(P);
    BN_CTX_end(ctx);
    BN_CTX_free(ctx);
}

int main(int argc, char **argv) {
    EC_GROUP *group = NULL;
    BIO *bio = NULL;

    assert(argc == 2);
    assert((bio = BIO_new(BIO_s_file())) != NULL);
    assert(BIO_read_filename(bio, argv[1]) == 1);
    assert((group = d2i_ECPKParameters_bio(bio, &group)) != NULL);
    printf("{");
    ec_stress(group, 0);
    printf(",");
    ec_stress(group, 1);
    printf("}\n");
    EC_GROUP_free(group);
    BIO_free(bio);
    return 0;
}

