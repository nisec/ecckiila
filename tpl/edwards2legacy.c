<%namespace file="fe_inv.c" import="vomit_op3"/>
/*-
 * from P projective Edwards to Q projective legacy: Q=P OK
% for comment in comments:
 * ${comment}
% endfor
 */
static void point_edwards2legacy(pt_prj_t *Q, const pt_prj_t *P) {
<% temps = [t for t in sorted(uniques) if t.startswith('t')] %>
% if temps:
    /* temporary variables */
    fe_t ${', '.join(temps)};
% endif
    /* constants */
% for i in set(input) & {'S', 'T', 'A', 'C', 'one'}:
    const limb_t *${i} = const_${i};
% endfor
    const limb_t *${input[0]} = P->X;
    const limb_t *${input[1]} = P->Y;
    const limb_t *${input[2]} = P->Z;
    limb_t *${output[0]} = Q->X;
    limb_t *${output[1]} = Q->Y;
    limb_t *${output[2]} = Q->T;
    limb_t *${output[3]} = Q->Z;

    /* the curve arith formula */\
    ${vomit_op3(lines)}
}
