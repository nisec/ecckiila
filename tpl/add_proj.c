<%namespace file="fe_inv.c" import="vomit_op3"/>
/*-
 * R := Q + P all projective.
 * R and Q same pointers OK
 * R and P same pointers not OK
% for comment in comments:
 * ${comment}
% endfor
 */
static void point_add_proj(pt_prj_t *R, const pt_prj_t *Q, const pt_prj_t *P) {
    /* temporary variables */
    fe_t ${', '.join([t for t in sorted(uniques) if t.startswith('t')])};
    /* constants */
% for i in set(input) & {'e', 'd', 'a', 'b', 'b3'}:
    const limb_t *${i} = const_${i};
% endfor
% if curve.is_edwards():
    /* set pointers for Edwards curve arith */
    const limb_t *${input[0]} = Q->X;
    const limb_t *${input[1]} = Q->Y;
    const limb_t *${input[2]} = Q->T;
    const limb_t *${input[3]} = Q->Z;
    const limb_t *${input[4]} = P->X;
    const limb_t *${input[5]} = P->Y;
    const limb_t *${input[6]} = P->T;
    const limb_t *${input[7]} = P->Z;
    limb_t *${output[0]} = R->X;
    limb_t *${output[1]} = R->Y;
    limb_t *${output[2]} = R->T;
    limb_t *${output[3]} = R->Z;
% else:
    /* set pointers for legacy curve arith */
    const limb_t *${input[0]} = Q->X;
    const limb_t *${input[1]} = Q->Y;
    const limb_t *${input[2]} = Q->Z;
    const limb_t *${input[3]} = P->X;
    const limb_t *${input[4]} = P->Y;
    const limb_t *${input[5]} = P->Z;
    limb_t *${output[0]} = R->X;
    limb_t *${output[1]} = R->Y;
    limb_t *${output[2]} = R->Z;
% endif

    /* the curve arith formula */\
    ${vomit_op3(lines)}
}
