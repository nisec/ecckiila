/* curve-related constants */
% for c in constants:
${tpl_fe(c,constants[c])}
% endfor

/* LUT for scalar multiplication by comb interleaving */
% if curve.is_edwards():
static const pt_aff_t lut_cmb[${len(points)}][${len(points[0])//3}] = {
% else:
static const pt_aff_t lut_cmb[${len(points)}][${len(points[0])//2}] = {
% endif
% while points:
{
% while points[0]:
{
{ ${tpl_coord(points[0].pop(0))},
% if curve.is_edwards():
{ ${tpl_coord(points[0].pop(0))},
% endif
{ ${tpl_coord(points[0].pop(0))}}${'' if not points else ','}
% endwhile
<% points.pop(0) %>\
}${'' if not points else ','}
% endwhile
};

## def for constants with names
<%def name="tpl_fe(name,l)">
static const limb_t ${name}[${len(l)}] = {
% for digit in l:
% if curve.word_size == 64:
UINT64_C(0x${'%016X' % digit})${'' if loop.last else ','}
% elif curve.word_size == 32:
UINT32_C(0x${'%08X' % digit})${'' if loop.last else ','}
% endif
% endfor
};
</%def>

## def for point coordinates
<%def name="tpl_coord(l)">
% for digit in l:
% if curve.word_size == 64:
UINT64_C(0x${'%016X' % digit})${'' if loop.last else ','}
% elif curve.word_size == 32:
UINT32_C(0x${'%08X' % digit})${'' if loop.last else ','}
% endif
% endfor
}
</%def>
