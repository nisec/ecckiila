<% if not curve.paths['fiat']: return STOP_RENDERING %>
% if curve.word_size == 64:
#ifdef RIG_NULL
#include "ecp_${curve.name}.h"
#endif
#include <stdint.h>
#include <string.h>
#define LIMB_BITS 64
#define LIMB_CNT ${curve.fe_nlimbs}
/* Field elements */
typedef uint64_t fe_t[LIMB_CNT];
typedef uint64_t limb_t;
% elif curve.word_size == 32:
#ifdef RIG_NULL
#include "ecp_${curve.name}.h"
#endif
#include <stdint.h>
#include <string.h>
#define LIMB_BITS 32
#define LIMB_CNT ${curve.fe_nlimbs}
/* Field elements */
typedef uint32_t fe_t[LIMB_CNT];
typedef uint32_t limb_t;
% endif

#ifdef OPENSSL_NO_ASM
#define FIAT_${curve.name.upper()}_NO_ASM
#endif

#define fe_copy(d, s) memcpy(d, s, sizeof(fe_t))
#define fe_set_zero(d) memset(d, 0, sizeof(fe_t))

/* Projective points */
typedef struct {
    fe_t X;
    fe_t Y;
% if curve.is_edwards():
    fe_t T;
% endif
    fe_t Z;
} pt_prj_t;

/* Affine points */
typedef struct {
    fe_t X;
    fe_t Y;
% if curve.is_edwards():
    fe_t T;
% endif
} pt_aff_t;

/* BEGIN verbatim fiat code https://github.com/mit-plv/fiat-crypto */
/*-
 * MIT License
 *
 * Copyright (c) 2015-2021 the fiat-crypto authors (see the AUTHORS file).
 * https://github.com/mit-plv/fiat-crypto/blob/master/AUTHORS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
${vomit(curve.paths['fiat'])}
/* END verbatim fiat code */

## curve-related constants
${curve.luts()}

## GF inversion
${curve.render_fe_inv()}

${curve.render_dbl()}
${curve.render_add_mixed()}
${curve.render_add_proj()}
% if curve.is_edwards():
${curve.render_edwards2legacy()}
${curve.render_legacy2edwards()}
% endif
${curve.scalar_mul()}

${curve.render_rigging()}

${curve.render_test()}

<%def name="vomit(path)">
<% with open(path, "r") as fp: text = fp.read() %>
${text}
</%def>
