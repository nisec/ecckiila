#ifdef LIB_TEST
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <inttypes.h>

#ifdef ECCKIILA_VALIDATION
#include <valgrind/memcheck.h>
#endif

static uint64_t rdtsc() {
#ifndef __ARM_NEON
    uint32_t lo, hi;

    __asm__ __volatile__ (
    "xorl %%eax, %%eax\n"
    "cpuid\n"
    "rdtsc\n"
    : "=a" (lo), "=d" (hi)
    :
    : "%ebx", "%ecx");

    return (uint64_t)hi << 32 | lo;
#else
    return 1;
#endif
}

#define CLOCK(x) (x >> 8 | (x ^ (x >> 7) ^ (x >> 6) ^ (x >> 2)) << 24)
static void RAND_bytes(unsigned char *b, int len) {
    static uint32_t theseed = 0xFEEDFACE;
    while (len--) {
        *b++ = (unsigned char)theseed;
        theseed = CLOCK(theseed);
    }
}

static int compare_ints(const void *a, const void *b) {
    const uint64_t *da = (const uint64_t *) a, *db = (const uint64_t *) b;
    return (*da > *db) - (*da < *db);
}

#ifdef ECCKIILA_VALIDATION
static void validation() {
    unsigned char x_b[${curve.fe_nbytes}];
    unsigned char y_b[${curve.fe_nbytes}];
    unsigned char k_b[${curve.fe_nbytes}];
    unsigned char l_b[${curve.fe_nbytes}];

    RAND_bytes(x_b, ${curve.fe_nbytes});
    RAND_bytes(y_b, ${curve.fe_nbytes});

    /* val: point_mul */
    RAND_bytes(k_b, ${curve.fe_nbytes});
    VALGRIND_MAKE_MEM_UNDEFINED(k_b, ${curve.fe_nbytes});
    ${curve.point_mul}(x_b, y_b, x_b, y_b, k_b);

    /* val: point_mul_g */
    RAND_bytes(k_b, ${curve.fe_nbytes});
    VALGRIND_MAKE_MEM_UNDEFINED(k_b, ${curve.fe_nbytes});
    ${curve.point_mul_g}(x_b, y_b, k_b);

    /* val: point_mul_two */
    RAND_bytes(k_b, ${curve.fe_nbytes});
    RAND_bytes(l_b, ${curve.fe_nbytes});
    VALGRIND_MAKE_MEM_UNDEFINED(k_b, ${curve.fe_nbytes});
    VALGRIND_MAKE_MEM_UNDEFINED(l_b, ${curve.fe_nbytes});
    ${curve.point_mul_two}(x_b, y_b, k_b, l_b, x_b, y_b);

    printf("Using VALGRIND_MAKE_MEM_UNDEFINED...\n");
}
#endif

#define NUM_TIMINGS (1 << 8)

static void benchmark() {
    int i;
    uint64_t start, stop;
    uint64_t *timings = NULL;
    unsigned char x_b[${curve.fe_nbytes}];
    unsigned char y_b[${curve.fe_nbytes}];
    unsigned char k_b[${curve.fe_nbytes}];
    unsigned char l_b[${curve.fe_nbytes}];

    assert((timings = malloc(sizeof(uint64_t)*NUM_TIMINGS)) != NULL);

    RAND_bytes(x_b, ${curve.fe_nbytes});
    RAND_bytes(y_b, ${curve.fe_nbytes});

    /* bench: inversion */
    for (i = 0; i < NUM_TIMINGS; i++) {
        fe_t b;
        RAND_bytes(k_b, ${curve.fe_nbytes});
        ${curve.fe_from_bytes}(b, k_b);
% if curve.fe_to_montgomery:
        ${curve.fe_to_montgomery}(b, b);
% endif
        start = rdtsc();
        ${curve.fe_inv}(b, b);
        stop = rdtsc();
        timings[i] = stop - start;
    }
    qsort(timings, NUM_TIMINGS, sizeof(uint64_t), compare_ints);
    printf("${curve.name}:fe_inv median cycles:%" PRIu64 "\n", timings[NUM_TIMINGS/2]);

    /* bench: point_mul */
    for (i = 0; i < NUM_TIMINGS; i++) {
        RAND_bytes(k_b, ${curve.fe_nbytes});
        start = rdtsc();
        ${curve.point_mul}(x_b, y_b, x_b, y_b, k_b);
        stop = rdtsc();
        timings[i] = stop - start;
    }
    qsort(timings, NUM_TIMINGS, sizeof(uint64_t), compare_ints);
    printf("${curve.name}:${curve.point_mul} median cycles:%" PRIu64 "\n", timings[NUM_TIMINGS/2]);

    /* bench: point_mul_g */
    for (i = 0; i < NUM_TIMINGS; i++) {
        RAND_bytes(k_b, ${curve.fe_nbytes});
        start = rdtsc();
        ${curve.point_mul_g}(x_b, y_b, k_b);
        stop = rdtsc();
        timings[i] = stop - start;
    }
    qsort(timings, NUM_TIMINGS, sizeof(uint64_t), compare_ints);
    printf("${curve.name}:${curve.point_mul_g} median cycles:%" PRIu64 "\n", timings[NUM_TIMINGS/2]);

    /* bench: point_mul_two */
    for (i = 0; i < NUM_TIMINGS; i++) {
        RAND_bytes(k_b, ${curve.fe_nbytes});
        RAND_bytes(l_b, ${curve.fe_nbytes});
        start = rdtsc();
        ${curve.point_mul_two}(x_b, y_b, k_b, l_b, x_b, y_b);
        stop = rdtsc();
        timings[i] = stop - start;
    }
    qsort(timings, NUM_TIMINGS, sizeof(uint64_t), compare_ints);
    printf("${curve.name}:${curve.point_mul_two} median cycles:%" PRIu64 "\n", timings[NUM_TIMINGS/2]);

    free(timings);
}

/* big endian string to little endian bytes */
void hex2bytes(unsigned char *out, const char *in) {
    int i;
    int len = strlen(in);

    assert(out != NULL);
    assert(in != NULL);
    assert(len == ${curve.fe_nbytes * 2});

    for(i = len - 2; i >= 0; i -= 2)
        assert(sscanf(in + i, "%02hhx", out++) == 1);
}

static void test_fe_inv(const char *in) {
    int i;
    fe_t a, inv, b;
    unsigned char abytes[${curve.fe_nbytes}];
    unsigned char bbytes[${curve.fe_nbytes}];
    unsigned char ibytes[${curve.fe_nbytes}];

    hex2bytes(abytes, in);
    ${curve.fe_from_bytes}(a, abytes);
% if curve.fe_to_montgomery:
    ${curve.fe_to_montgomery}(a, a);
% endif
    ${curve.fe_inv}(inv, a);
    ${curve.fe_mul}(b, a, inv);
% if curve.fe_from_montgomery:
    ${curve.fe_from_montgomery}(b, b);
% endif
    ${curve.fe_to_bytes}(bbytes, b);

    /* sanity check inverse */
    assert(bbytes[0] == 1);
    for(i = 1; i < ${curve.fe_nbytes}; i++)
        assert(bbytes[i] == 0);

    /* output inverse */
% if curve.fe_from_montgomery:
    ${curve.fe_from_montgomery}(inv, inv);
% endif
    ${curve.fe_to_bytes}(ibytes, inv);

    for (i = ${curve.fe_nbytes - 1}; i >= 0; i--)
        printf("%02x", ibytes[i]);
    printf("#\n");
}

static void test_scmul(const char *a, const char *b, const char *x, const char *y) {
    int i;
    unsigned char b_a[${curve.fe_nbytes}];
    unsigned char b_b[${curve.fe_nbytes}];
    unsigned char b_x[${curve.fe_nbytes}];
    unsigned char b_y[${curve.fe_nbytes}];

    assert(a != NULL || b != NULL);

    if (a != NULL && b == NULL) {
      /* convert strings to hex */
      hex2bytes(b_a, a);
      /* run KAT for fixed scalar multiplication */
      ${curve.point_mul_g}(b_x, b_y, b_a);
    } else if (a == NULL && b != NULL){
        assert(x != NULL && y != NULL);
        hex2bytes(b_x, x);
        hex2bytes(b_y, y);
        /* convert strings to hex */
        hex2bytes(b_b, b);
        /* run KAT for variable scalar multiplication */
        ${curve.point_mul}(b_x, b_y, b_b, b_x, b_y);
    } else {
        assert(x != NULL && y != NULL);
        hex2bytes(b_x, x);
        hex2bytes(b_y, y);
        /* convert strings to hex */
        hex2bytes(b_a, a);
        hex2bytes(b_b, b);
        /* run KAT for simultaneous scalar multiplication */
        ${curve.point_mul_two}(b_x, b_y, b_a, b_b, b_x, b_y);
    }

    /* output resulting point */
    for (i = ${curve.fe_nbytes - 1}; i >= 0; i--)
        printf("%02x", b_x[i]);
    printf("#");
    for (i = ${curve.fe_nbytes - 1}; i >= 0; i--)
        printf("%02x", b_y[i]);
    printf("#\n");
}

#define STRESS_ITS (1 << 16)

static void test_stress(int is_g) {
    int i, j;
    unsigned char b_a[${curve.fe_nbytes}] = {0};
    unsigned char b_x[${curve.fe_nbytes}];
    unsigned char b_y[${curve.fe_nbytes}];

    /* fetch g */
    b_a[0] = 1;
    ${curve.point_mul_g}(b_x, b_y, b_a);

    for (i = 0; i < STRESS_ITS; i++) {
        /* new scalar is XOR of previous coords */
        for (j = 0; j < ${curve.fe_nbytes}; j++)
            b_a[j] = b_x[j] ^ b_y[j];
        /* take another step on the walk */
        if (is_g)
            ${curve.point_mul_g}(b_x, b_y, b_a);
        else
            ${curve.point_mul}(b_x, b_y, b_a, b_x, b_y);
    }

    /* output resulting point */
    for (i = ${curve.fe_nbytes - 1}; i >= 0; i--)
        printf("%02x", b_x[i]);
    printf("#");
    for (i = ${curve.fe_nbytes - 1}; i >= 0; i--)
        printf("%02x", b_y[i]);
    printf("#\n");
}

int main(int argc, char **argv) {

    assert(argc > 1 && argv[1][0] == '-');

    if (argv[1][1] == 'i') {
        assert(argc == 3);
        test_fe_inv(argv[2]);
    }

    if (argv[1][1] == 's') {
        assert(argc == 5);
        test_scmul(NULL, argv[2], argv[3], argv[4]);
    }

    if (argv[1][1] == 'S') {
        assert(argc == 3);
        test_scmul(argv[2], NULL, NULL, NULL);
    }

    if (argv[1][1] == '2') {
        assert(argc == 6);
        test_scmul(argv[2], argv[3], argv[4], argv[5]);
    }

    if (argv[1][1] == 'b') {
        assert(argc == 2);
        benchmark();
    }

    if (argv[1][1] == 't') {
        assert(argc == 2);
        test_stress(0);
    }

    if (argv[1][1] == 'T') {
        assert(argc == 2);
        test_stress(1);
    }

#ifdef ECCKIILA_VALIDATION
    if (argv[1][1] == 'V') {
        assert(argc == 2);
        validation();
    }
#endif

    return 0;
}
#endif
