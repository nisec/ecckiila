<%namespace file="fe_inv.c" import="vomit_op3"/>
% if not curve.fe_to_montgomery and not curve.is_edwards():
/*-
 * out1 = (arg1 == 0) ? 0 : nz
 * NB: this is not a "mod p equiv" 0, but literal 0
 * NB: this is not a real fiat-crypto function, just named that way for consistency.
 */
static void fiat_${curve.name}_nonzero(limb_t *out1, const fe_t arg1) {
    limb_t x1 = 0;
    int i;

    for (i = 0; i < LIMB_CNT; i++)
        x1 |= arg1[i];
    *out1 = x1;
}
% endif

/*-
 * R := Q + P where R and Q are projective, P affine.
 * R and Q same pointers OK
 * R and P same pointers not OK
% for comment in comments:
 * ${comment}
% endfor
 */
static void point_add_mixed(pt_prj_t *R, const pt_prj_t *Q, const pt_aff_t *P) {
    /* temporary variables */
    fe_t ${', '.join([t for t in sorted(uniques) if t.startswith('t')])};
    /* constants */
% for i in set(input) & {'e', 'd', 'a', 'b', 'b3'}:
    const limb_t *${i} = const_${i};
% endfor
% if curve.is_edwards():
    /* set pointers for Edwards curve arith */
    const limb_t *${input[0]} = Q->X;
    const limb_t *${input[1]} = Q->Y;
    const limb_t *${input[2]} = Q->T;
    const limb_t *${input[3]} = Q->Z;
    const limb_t *${input[4]} = P->X;
    const limb_t *${input[5]} = P->Y;
    const limb_t *${input[6]} = P->T;
    ## no z-coord for second point
    limb_t *${output[0]} = R->X;
    limb_t *${output[1]} = R->Y;
    limb_t *${output[2]} = R->T;
    limb_t *${output[3]} = R->Z;
% else:
    /* set pointers for legacy curve arith */
    const limb_t *${input[0]} = Q->X;
    const limb_t *${input[1]} = Q->Y;
    const limb_t *${input[2]} = Q->Z;
    const limb_t *${input[3]} = P->X;
    const limb_t *${input[4]} = P->Y;
    ## no z-coord for second point
    fe_t ${output[0]};
    fe_t ${output[1]};
    fe_t ${output[2]};
    limb_t nz;

    /* check P for affine inf */
    ${curve.fe_nonzero}(&nz, P->Y);
% endif

    /* the curve arith formula */\
    ${vomit_op3(lines)}
% if not curve.is_edwards():
    /* if P is inf, throw all that away and take Q */
    ${curve.fe_selectznz}(R->X, nz, Q->X, ${output[0]});
    ${curve.fe_selectznz}(R->Y, nz, Q->Y, ${output[1]});
    ${curve.fe_selectznz}(R->Z, nz, Q->Z, ${output[2]});
% endif
}
