/*-
 * Finite field inversion.
 % if curve.fe_to_montgomery:
 * Computed with Bernstein-Yang algorithm.
 * https://tches.iacr.org/index.php/TCHES/article/view/8298
 * Based on https://github.com/mit-plv/fiat-crypto/tree/master/inversion/c
% else:
 * Computed with exponentiation via FLT.
% for comment in comments:
 * ${comment}
% endfor
% endif
 * NB: this is not a real fiat-crypto function, just named that way for consistency.
 */
static void ${curve.fe_inv}(fe_t output, const fe_t t1) {
% if curve.fe_to_montgomery:
    int i;
    fe_t v1, r1, v2;
    limb_t *r2 = output;
    limb_t f1[LIMB_CNT + 1], g1[LIMB_CNT + 1], f2[LIMB_CNT + 1], g2[LIMB_CNT + 1];
    limb_t d2, d1 = 1;

    fe_copy(g1, t1);
    g1[LIMB_CNT] = 0;
    fe_copy(f1, const_psat);
    f1[LIMB_CNT] = 0;
    fe_copy(r1, const_one);
    fe_set_zero(v1);

    /* ${curve.divstep_its} divstep iterations */
    for (i = 0; i < ${curve.divstep_its // 2}; i++) {
        ${curve.fe_divstep}(&d2, f2, g2, v2, r2, d1, f1, g1, v1, r1);
        ${curve.fe_divstep}(&d1, f1, g1, v1, r1, d2, f2, g2, v2, r2);
    }

% if curve.divstep_its & 1:
    <% var_v, var_f = 'v2', 'f2' %>
    ${curve.fe_divstep}(&d2, f2, g2, v2, r2, d1, f1, g1, v1, r1);
% else:
    <% var_v, var_f = 'v1', 'f1' %>
% endif
    ${curve.fe_opp}(output, ${var_v});
    ${curve.fe_selectznz}(output, ${var_f}[LIMB_CNT] >> (LIMB_BITS - 1), ${var_v}, output);
    ${curve.fe_mul}(output, output, const_divstep);
% else:
    <% if 't1' in uniques: uniques.remove('t1') %>
    int i;
    /* temporary variables */
    fe_t acc, ${', '.join([t for t in sorted(uniques) if t.startswith('t')])};

    ${vomit_op3(lines, loop=True)}
% endif
}

<%def name="vomit_op3(lines, loop=False)">
% while lines:
<%
    line = lines.pop(0)
    its = 1
    while loop and lines and lines[0] == line:
        lines.pop(0)
        its += 1
%>\
    % if its > 1:
    for (i = 0; i < ${its}; i++)
    % endif
    % if line[2] == '*':
        % if line[1] == line[3]:
            ${curve.fe_sqr}(${line[0]}, ${line[1]});
        % else:
            ${curve.fe_mul}(${line[0]}, ${line[1]}, ${line[3]});
        % endif
    % elif line[2] == '+':
        ${curve.fe_add}(${line[0]}, ${line[1]}, ${line[3]});
    % elif line[2] == '-':
        ${curve.fe_sub}(${line[0]}, ${line[1]}, ${line[3]});
    % else:
        ## should never happen
        /* ERROR! Line omitted: ${line} */
        <% assert(0) %>
    % endif
% endwhile
</%def>
