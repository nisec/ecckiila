<%namespace file="fe_inv.c" import="vomit_op3"/>
/*-
 * from P affine legacy to Q projective Edwards: Q=P not OK
% for comment in comments:
 * ${comment}
% endfor
 */
static void point_legacy2edwards(pt_prj_t *Q, const pt_aff_t *P) {
<% temps = [t for t in sorted(uniques) if t.startswith('t')] %>
% if temps:
    /* temporary variables */
    fe_t ${', '.join(temps)};
% endif
    /* constants */
% for i in set(input) & {'S', 'T', 'A', 'C', 'one'}:
    const limb_t *${i} = const_${i};
% endfor
    const limb_t *${input[0]} = P->X;
    const limb_t *${input[1]} = P->Y;
    limb_t *${output[0]} = Q->X;
    limb_t *${output[1]} = Q->Y;
    limb_t *${output[2]} = Q->T;
    limb_t *${output[3]} = Q->Z;

    /* the curve arith formula */\
    ${vomit_op3(lines)}
}
