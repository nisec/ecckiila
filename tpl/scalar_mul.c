/* constants */
#define RADIX ${curve.radix}
#define DRADIX (1 << RADIX)
#define DRADIX_WNAF ((DRADIX) << 1)

/*-
 * precomp for wnaf scalar multiplication:
 * precomp[0] = 1P
 * precomp[1] = 3P
 * precomp[2] = 5P
 * precomp[3] = 7P
 * precomp[4] = 9P
 * ...
 */
static void precomp_wnaf(pt_prj_t precomp[DRADIX/2], const pt_aff_t *P)
{
    int i;

## conditionally apply legacy -> edwards map
% if curve.is_edwards():
    /* move from legacy affine to Edwards projective */
    point_legacy2edwards(&precomp[0], P);
% else:
    fe_copy(precomp[0].X, P->X);
    fe_copy(precomp[0].Y, P->Y);
    fe_copy(precomp[0].Z, const_one);
% endif
    point_double(&precomp[DRADIX/2 - 1], &precomp[0]);

    for (i = 1; i < DRADIX/2; i++)
        point_add_proj(&precomp[i], &precomp[DRADIX/2 - 1], &precomp[i - 1]);
}

/* fetch a scalar bit */
static int scalar_get_bit(const unsigned char in[${curve.fe_nbytes}], int idx) {
    int widx, rshift;

    widx = idx >> 3;
    rshift = idx & 0x7;

    if (idx < 0 || widx >= ${curve.fe_nbytes})
        return 0;

    return (in[widx] >> rshift) & 0x1;
}

/*-
 * Compute "regular" wnaf representation of a scalar.
 * See "Exponent Recoding and Regular Exponentiation Algorithms",
 * Tunstall et al., AfricaCrypt 2009, Alg 6.
 * It forces an odd scalar and outputs digits in
 * {\pm 1, \pm 3, \pm 5, \pm 7, \pm 9, ...}
 * i.e. signed odd digits with _no zeroes_ -- that makes it "regular".
 */
static void scalar_rwnaf(int8_t out[${8 * curve.fe_nbytes // curve.radix + 1}], const unsigned char in[${curve.fe_nbytes}]) {
    int i;
    int8_t window, d;

    window = (in[0] & (DRADIX_WNAF - 1)) | 1;
    for (i = 0; i < ${8 * curve.fe_nbytes // curve.radix}; i++) {
        d = (window & (DRADIX_WNAF - 1)) - DRADIX;
        out[i] = d;
        window = (window - d) >> RADIX;
% for i in range(1, curve.radix + 1):
        window += scalar_get_bit(in, (i + 1) * RADIX + ${i}) << ${i};
% endfor
    }
    out[i] = window;
}

/*-
 * Compute "textbook" wnaf representation of a scalar.
 * NB: not constant time
 */
static void scalar_wnaf(int8_t out[${8 * curve.fe_nbytes + 1}], const unsigned char in[${curve.fe_nbytes}]) {
    int i;
    int8_t window, d;

    window = in[0] & (DRADIX_WNAF - 1);
    for (i = 0; i < ${8 * curve.fe_nbytes + 1}; i++) {
        d = 0;
        if ((window & 1) && ((d = window & (DRADIX_WNAF - 1)) & DRADIX))
            d -= DRADIX_WNAF;
        out[i] = d;
        window = (window - d) >> 1;
        window += scalar_get_bit(in, i + 1 + RADIX) << RADIX;
    }
}

/*-
 * Simultaneous scalar multiplication: interleaved "textbook" wnaf.
 * NB: not constant time
 */
static void var_smul_wnaf_two(pt_aff_t *out, const unsigned char a[${curve.fe_nbytes}], const unsigned char b[${curve.fe_nbytes}], const pt_aff_t *P) {
    int i, d, is_neg, is_inf = 1, flipped = 0;
    int8_t anaf[${8 * curve.fe_nbytes + 1}] = {0};
    int8_t bnaf[${8 * curve.fe_nbytes + 1}] = {0};
    pt_prj_t Q = { {0}, {0}, {0}
% if curve.is_edwards():
	, {0}
% endif
    };
    pt_prj_t precomp[DRADIX/2];

    precomp_wnaf(precomp, P);
    scalar_wnaf(anaf, a);
    scalar_wnaf(bnaf, b);

    for (i = ${8 * curve.fe_nbytes}; i >= 0; i--) {
        if (!is_inf)
            point_double(&Q, &Q);
        if ((d = bnaf[i])) {
            if ((is_neg = d < 0) != flipped) {
% if curve.is_edwards():
                ${curve.fe_opp}(Q.X, Q.X);
                ${curve.fe_opp}(Q.T, Q.T);
% else:
                ${curve.fe_opp}(Q.Y, Q.Y);
% endif
                flipped ^= 1;
            }
            d = (is_neg) ? (-d - 1) >> 1 : (d - 1) >> 1;
            if (is_inf) {
                /* initialize accumulator */
% if curve.is_edwards():
                fe_copy(Q.X, &precomp[d].X);
                fe_copy(Q.Y, &precomp[d].Y);
                fe_copy(Q.T, &precomp[d].T);
                fe_copy(Q.Z, &precomp[d].Z);
% else:
                fe_copy(Q.X, &precomp[d].X);
                fe_copy(Q.Y, &precomp[d].Y);
                fe_copy(Q.Z, &precomp[d].Z);
% endif
                is_inf = 0;
            } else
                point_add_proj(&Q, &Q, &precomp[d]);
        }
        if ((d = anaf[i])) {
            if ((is_neg = d < 0) != flipped) {
% if curve.is_edwards():
                ${curve.fe_opp}(Q.X, Q.X);
                ${curve.fe_opp}(Q.T, Q.T);
% else:
                ${curve.fe_opp}(Q.Y, Q.Y);
% endif
                flipped ^= 1;
            }
            d = (is_neg) ? (-d - 1) >> 1 : (d - 1) >> 1;
            if (is_inf) {
                /* initialize accumulator */
% if curve.is_edwards():
                fe_copy(Q.X, &lut_cmb[0][d].X);
                fe_copy(Q.Y, &lut_cmb[0][d].Y);
                fe_copy(Q.T, &lut_cmb[0][d].T);
                fe_copy(Q.Z, const_one);
% else:
                fe_copy(Q.X, &lut_cmb[0][d].X);
                fe_copy(Q.Y, &lut_cmb[0][d].Y);
                fe_copy(Q.Z, const_one);
% endif
                is_inf = 0;
            } else
                point_add_mixed(&Q, &Q, &lut_cmb[0][d]);
        }
    }

    if (is_inf) {
        /* initialize accumulator to inf: all-zero scalars */
% if curve.is_edwards():
        fe_set_zero(Q.X);
        fe_copy(Q.Y, const_one);
        fe_set_zero(Q.T);
        fe_copy(Q.Z, const_one);
% else:
        fe_set_zero(Q.X);
        fe_copy(Q.Y, const_one);
        fe_set_zero(Q.Z);
% endif
    }

    if (flipped) {
        /* correct sign */
% if curve.is_edwards():
        ${curve.fe_opp}(Q.X, Q.X);
        ${curve.fe_opp}(Q.T, Q.T);
% else:
        ${curve.fe_opp}(Q.Y, Q.Y);
% endif
    }

## conditionally emit Edwards inverse map
% if curve.is_edwards():
    /* move from Edwards projective to legacy projective */
    point_edwards2legacy(&Q, &Q);
% endif
    /* convert to affine -- NB depends on coordinate system */
    ${curve.fe_inv}(Q.Z, Q.Z);
    ${curve.fe_mul}(out->X, Q.X, Q.Z);
    ${curve.fe_mul}(out->Y, Q.Y, Q.Z);
}

/*-
 * Variable point scalar multiplication with "regular" wnaf.
 * Here "regular" means _no zeroes_, so the sequence of
 * EC arithmetic ops is fixed.
% if curve.clear_cofactor:
 * Includes cofactor clearing by default.
% endif
 */
static void var_smul_rwnaf(pt_aff_t *out, const unsigned char scalar[${curve.fe_nbytes}], const pt_aff_t *P) {
    int i, j, d, diff, is_neg;
    int8_t rnaf[${8 * curve.fe_nbytes // curve.radix + 1}] = {0};
    pt_prj_t Q = { {0}, {0}, {0}
% if curve.is_edwards():
        , {0}
% endif
    }, lut = { {0}, {0}, {0}
% if curve.is_edwards():
        , {0}
% endif
    };
    pt_prj_t precomp[DRADIX/2];

    precomp_wnaf(precomp, P);
    scalar_rwnaf(rnaf, scalar);

#if defined(_MSC_VER)
    /* result still unsigned: yes we know */
#pragma warning(push)
#pragma warning(disable : 4146)
#endif

    /* initialize accumulator to high digit */
    d = (rnaf[${8 * curve.fe_nbytes // curve.radix}] - 1) >> 1;
    for (j = 0; j < DRADIX / 2; j++) {
        diff = (1 - (-(d ^ j) >> (8 * sizeof(int) - 1))) & 1;
        ${curve.fe_selectznz}(Q.X, diff, Q.X, precomp[j].X);
        ${curve.fe_selectznz}(Q.Y, diff, Q.Y, precomp[j].Y);
% if curve.is_edwards():
        ${curve.fe_selectznz}(Q.T, diff, Q.T, precomp[j].T);
% endif
        ${curve.fe_selectznz}(Q.Z, diff, Q.Z, precomp[j].Z);
    }

    for (i = ${8 * curve.fe_nbytes // curve.radix - 1}; i >= 0; i--) {
        for (j = 0; j < RADIX; j++)
            point_double(&Q, &Q);
        d = rnaf[i];
        /* is_neg = (d < 0) ? 1 : 0 */
        is_neg = (d >> (8 * sizeof(int) - 1)) & 1;
        /* d = abs(d) */
        d = (d ^ -is_neg) + is_neg;
        d = (d - 1) >> 1;
        for (j = 0; j < DRADIX / 2; j++) {
            diff = (1 - (-(d ^ j) >> (8 * sizeof(int) - 1))) & 1;
            ${curve.fe_selectznz}(lut.X, diff, lut.X, precomp[j].X);
            ${curve.fe_selectznz}(lut.Y, diff, lut.Y, precomp[j].Y);
% if curve.is_edwards():
            ${curve.fe_selectznz}(lut.T, diff, lut.T, precomp[j].T);
% endif
            ${curve.fe_selectznz}(lut.Z, diff, lut.Z, precomp[j].Z);
        }
        /* negate lut point if digit is negative */
% if curve.is_edwards():
        ${curve.fe_opp}(out->X, lut.X);
        ${curve.fe_opp}(out->T, lut.T);
        ${curve.fe_selectznz}(lut.X, is_neg, lut.X, out->X);
        ${curve.fe_selectznz}(lut.T, is_neg, lut.T, out->T);
% else:
        ${curve.fe_opp}(out->Y, lut.Y);
        ${curve.fe_selectznz}(lut.Y, is_neg, lut.Y, out->Y);
% endif
        point_add_proj(&Q, &Q, &lut);
    }

#if defined(_MSC_VER)
#pragma warning(pop)
#endif

    /* conditionally subtract P if the scalar was even */
% if curve.is_edwards():
    ${curve.fe_opp}(lut.X, precomp[0].X);
    fe_copy(lut.Y, precomp[0].Y);
    ${curve.fe_opp}(lut.T, precomp[0].T);
    fe_copy(lut.Z, precomp[0].Z);
    point_add_proj(&lut, &lut, &Q);
    ${curve.fe_selectznz}(Q.X, scalar[0] & 1, lut.X, Q.X);
    ${curve.fe_selectznz}(Q.Y, scalar[0] & 1, lut.Y, Q.Y);
    ${curve.fe_selectznz}(Q.T, scalar[0] & 1, lut.T, Q.T);
    ${curve.fe_selectznz}(Q.Z, scalar[0] & 1, lut.Z, Q.Z);
% else:
    fe_copy(lut.X, precomp[0].X);
    ${curve.fe_opp}(lut.Y, precomp[0].Y);
    fe_copy(lut.Z, precomp[0].Z);
    point_add_proj(&lut, &lut, &Q);
    ${curve.fe_selectznz}(Q.X, scalar[0] & 1, lut.X, Q.X);
    ${curve.fe_selectznz}(Q.Y, scalar[0] & 1, lut.Y, Q.Y);
    ${curve.fe_selectznz}(Q.Z, scalar[0] & 1, lut.Z, Q.Z);
% endif

## conditionally kill / clear cofactor
% if curve.clear_cofactor:
    /* kill / clear cofactor */
    for (i = 0; i < ${curve.h.bit_length() - 1}; i++)
        point_double(&Q, &Q);
% endif

## conditionally emit Edwards inverse map
% if curve.is_edwards():
    /* move from Edwards projective to legacy projective */
    point_edwards2legacy(&Q, &Q);
% endif
    /* convert to affine -- NB depends on coordinate system */
    ${curve.fe_inv}(Q.Z, Q.Z);
    ${curve.fe_mul}(out->X, Q.X, Q.Z);
    ${curve.fe_mul}(out->Y, Q.Y, Q.Z);
}

/*-
 * Fixed scalar multiplication: comb with interleaving.
 */
static void fixed_smul_cmb(pt_aff_t *out, const unsigned char scalar[${curve.fe_nbytes}])
{
    int i, j, k, d, diff, is_neg = 0;
    int8_t rnaf[${8 * curve.fe_nbytes // curve.radix + 1}] = {0};
    pt_prj_t Q = { {0}, {0}, {0}
% if curve.is_edwards():
        , {0}
% endif
    }, R = { {0}, {0}, {0}
% if curve.is_edwards():
        , {0}
% endif
    };
    pt_aff_t lut = { {0}, {0}
% if curve.is_edwards():
        , {0}
% endif
    };

    scalar_rwnaf(rnaf, scalar);

    /* initalize accumulator to inf */
% if curve.is_edwards():
    fe_set_zero(Q.X);
    fe_copy(Q.Y, const_one);
    fe_set_zero(Q.T);
    fe_copy(Q.Z, const_one);
% else:
    fe_set_zero(Q.X);
    fe_copy(Q.Y, const_one);
    fe_set_zero(Q.Z);
% endif

#if defined(_MSC_VER)
    /* result still unsigned: yes we know */
#pragma warning(push)
#pragma warning(disable : 4146)
#endif

    for (i = ${curve.cmb_size - 1}; i >= 0; i--) {
        for (j = 0; i != ${curve.cmb_size - 1} && j < RADIX; j++)
            point_double(&Q, &Q);
        for (j = 0; j < ${curve.cmb_words}; j++) {
            if (j * ${curve.cmb_size} + i > ${8 * curve.fe_nbytes // curve.radix})
                continue;
              d = rnaf[j * ${curve.cmb_size} + i];
            /* is_neg = (d < 0) ? 1 : 0 */
            is_neg = (d >> (8 * sizeof(int) - 1)) & 1;
            /* d = abs(d) */
            d = (d ^ -is_neg) + is_neg;
            d = (d - 1) >> 1;
            for (k = 0; k < DRADIX / 2; k++) {
                diff = (1 - (-(d ^ k) >> (8 * sizeof(int) - 1))) & 1;
                ${curve.fe_selectznz}(lut.X, diff, lut.X, lut_cmb[j][k].X);
                ${curve.fe_selectznz}(lut.Y, diff, lut.Y, lut_cmb[j][k].Y);
% if curve.is_edwards():
                ${curve.fe_selectznz}(lut.T, diff, lut.T, lut_cmb[j][k].T);
% endif
            }
            /* negate lut point if digit is negative */
  % if curve.is_edwards():
            ${curve.fe_opp}(out->X, lut.X);
            ${curve.fe_opp}(out->T, lut.T);
            ${curve.fe_selectznz}(lut.X, is_neg, lut.X, out->X);
            ${curve.fe_selectznz}(lut.T, is_neg, lut.T, out->T);
  % else:
            ${curve.fe_opp}(out->Y, lut.Y);
            ${curve.fe_selectznz}(lut.Y, is_neg, lut.Y, out->Y);
  % endif
            point_add_mixed(&Q, &Q, &lut);
        }
    }

#if defined(_MSC_VER)
#pragma warning(pop)
#endif

    /* conditionally subtract P if the scalar was even */
% if curve.is_edwards():
    ${curve.fe_opp}(lut.X, lut_cmb[0][0].X);
    fe_copy(lut.Y, lut_cmb[0][0].Y);
    ${curve.fe_opp}(lut.T, lut_cmb[0][0].T);
    point_add_mixed(&R, &Q, &lut);
    ${curve.fe_selectznz}(Q.X, scalar[0] & 1, R.X, Q.X);
    ${curve.fe_selectznz}(Q.Y, scalar[0] & 1, R.Y, Q.Y);
    ${curve.fe_selectznz}(Q.T, scalar[0] & 1, R.T, Q.T);
    ${curve.fe_selectznz}(Q.Z, scalar[0] & 1, R.Z, Q.Z);
% else:
    fe_copy(lut.X, lut_cmb[0][0].X);
    ${curve.fe_opp}(lut.Y, lut_cmb[0][0].Y);
    point_add_mixed(&R, &Q, &lut);
    ${curve.fe_selectznz}(Q.X, scalar[0] & 1, R.X, Q.X);
    ${curve.fe_selectznz}(Q.Y, scalar[0] & 1, R.Y, Q.Y);
    ${curve.fe_selectznz}(Q.Z, scalar[0] & 1, R.Z, Q.Z);
% endif

## conditionally emit Edwards inverse map
% if curve.is_edwards():
    /* move from Edwards projective to legacy projective */
    point_edwards2legacy(&Q, &Q);
% endif
    /* convert to affine -- NB depends on coordinate system */
    ${curve.fe_inv}(Q.Z, Q.Z);
    ${curve.fe_mul}(out->X, Q.X, Q.Z);
    ${curve.fe_mul}(out->Y, Q.Y, Q.Z);
}

/*-
 * Wrapper: simultaneous scalar mutiplication.
 * outx, outy := a * G + b * P
 * where P = (inx, iny).
 * Everything is LE byte ordering.
 */
#ifndef RIG_NULL
static
#endif
void ${curve.point_mul_two}(unsigned char outx[${curve.fe_nbytes}], unsigned char outy[${curve.fe_nbytes}], const unsigned char a[${curve.fe_nbytes}], const unsigned char b[${curve.fe_nbytes}], const unsigned char inx[${curve.fe_nbytes}], const unsigned char iny[${curve.fe_nbytes}]) {
    pt_aff_t P;

    ${curve.fe_from_bytes}(P.X, inx);
    ${curve.fe_from_bytes}(P.Y, iny);
% if curve.fe_to_montgomery:
    ${curve.fe_to_montgomery}(P.X, P.X);
    ${curve.fe_to_montgomery}(P.Y, P.Y);
% endif
    /* simultaneous scalar multiplication */
    var_smul_wnaf_two(&P, a, b, &P);

% if curve.fe_from_montgomery:
    ${curve.fe_from_montgomery}(P.X, P.X);
    ${curve.fe_from_montgomery}(P.Y, P.Y);
% endif
    ${curve.fe_to_bytes}(outx, P.X);
    ${curve.fe_to_bytes}(outy, P.Y);
}

/*-
 * Wrapper: fixed scalar mutiplication.
 * outx, outy := scalar * G
 * Everything is LE byte ordering.
 */
#ifndef RIG_NULL
static
#endif
void ${curve.point_mul_g}(unsigned char outx[${curve.fe_nbytes}], unsigned char outy[${curve.fe_nbytes}], const unsigned char scalar[${curve.fe_nbytes}]) {
    pt_aff_t P;

    /* fixed scmul function */
    fixed_smul_cmb(&P, scalar);
% if curve.fe_from_montgomery:
    ${curve.fe_from_montgomery}(P.X, P.X);
    ${curve.fe_from_montgomery}(P.Y, P.Y);
% endif
    ${curve.fe_to_bytes}(outx, P.X);
    ${curve.fe_to_bytes}(outy, P.Y);
}

/*-
 * Wrapper: variable point scalar mutiplication.
% if curve.clear_cofactor:
 * Includes cofactor clearing / killing by default.
 * outx, outy := cofactor * scalar * P
% else:
 * outx, outy := scalar * P
% endif
 * where P = (inx, iny).
 * Everything is LE byte ordering.
 */
#ifndef RIG_NULL
static
#endif
void ${curve.point_mul}(unsigned char outx[${curve.fe_nbytes}], unsigned char outy[${curve.fe_nbytes}], const unsigned char scalar[${curve.fe_nbytes}], const unsigned char inx[${curve.fe_nbytes}], const unsigned char iny[${curve.fe_nbytes}]) {
    pt_aff_t P;

    ${curve.fe_from_bytes}(P.X, inx);
    ${curve.fe_from_bytes}(P.Y, iny);
% if curve.fe_to_montgomery:
    ${curve.fe_to_montgomery}(P.X, P.X);
    ${curve.fe_to_montgomery}(P.Y, P.Y);
% endif
    /* var scmul function */
    var_smul_rwnaf(&P, scalar, &P);
% if curve.fe_from_montgomery:
    ${curve.fe_from_montgomery}(P.X, P.X);
    ${curve.fe_from_montgomery}(P.Y, P.Y);
% endif
    ${curve.fe_to_bytes}(outx, P.X);
    ${curve.fe_to_bytes}(outy, P.Y);
}
