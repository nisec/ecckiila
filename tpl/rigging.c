#ifdef RIG_NSS
#undef RADIX
#include "ecp.h"
#include "mpi-priv.h"
#include "mplogic.h"

/*-
 * reverse bytes -- total hack
 */
#define MP_BE2LE(a) do { ${'\\'}
    unsigned char z_bswap; ${'\\'}
<% left, right = 0, curve.fe_nbytes - 1 %>\
% while left < right:
    z_bswap = a[${left}]; ${'\\'}
    a[${left}] = a[${right}]; ${'\\'}
    a[${right}] = z_bswap; ${'\\'}
<% left, right = left + 1, right - 1 %>\
% endwhile
} while(0)

static mp_err point_mul_g_${curve.name}_wrap(const mp_int *n,
                               mp_int *out_x, mp_int *out_y,
                               const ECGroup *group)
{
    unsigned char b_x[${curve.fe_nbytes}];
    unsigned char b_y[${curve.fe_nbytes}];
    unsigned char b_n[${curve.fe_nbytes}];
    mp_err res;

    ARGCHK(n != NULL && out_x != NULL && out_y != NULL, MP_BADARG);

    /* fail on out of range scalars */
    if (mpl_significant_bits(n) > ${curve.p.bit_length()}
        || mp_cmp_z(n) != MP_GT)
        return MP_RANGE;

    MP_CHECKOK(mp_to_fixlen_octets(n, b_n, ${curve.fe_nbytes}));
    MP_BE2LE(b_n);
    ${curve.point_mul_g}(b_x, b_y, b_n);
    MP_BE2LE(b_x);
    MP_BE2LE(b_y);
    MP_CHECKOK(mp_read_unsigned_octets(out_x, b_x, ${curve.fe_nbytes}));
    MP_CHECKOK(mp_read_unsigned_octets(out_y, b_y, ${curve.fe_nbytes}));

CLEANUP:
    return res;
}

static mp_err point_mul_${curve.name}_wrap(const mp_int *n,
                          const mp_int *in_x, const mp_int *in_y,
                          mp_int *out_x, mp_int *out_y,
                          const ECGroup *group)
{
    unsigned char b_x[${curve.fe_nbytes}];
    unsigned char b_y[${curve.fe_nbytes}];
    unsigned char b_n[${curve.fe_nbytes}];
    mp_err res;

    ARGCHK(n != NULL && in_x != NULL && in_y != NULL && out_x != NULL
           && out_y != NULL, MP_BADARG);

   /* fail on out of range scalars */
    if (mpl_significant_bits(n) > ${curve.p.bit_length()}
        || mp_cmp_z(n) != MP_GT)
        return MP_RANGE;

    MP_CHECKOK(mp_to_fixlen_octets(n, b_n, ${curve.fe_nbytes}));
    MP_CHECKOK(mp_to_fixlen_octets(in_x, b_x, ${curve.fe_nbytes}));
    MP_CHECKOK(mp_to_fixlen_octets(in_y, b_y, ${curve.fe_nbytes}));
    MP_BE2LE(b_x);
    MP_BE2LE(b_y);
    MP_BE2LE(b_n);
    ${curve.point_mul}(b_x, b_y, b_n, b_x, b_y);
    MP_BE2LE(b_x);
    MP_BE2LE(b_y);
    MP_CHECKOK(mp_read_unsigned_octets(out_x, b_x, ${curve.fe_nbytes}));
    MP_CHECKOK(mp_read_unsigned_octets(out_y, b_y, ${curve.fe_nbytes}));

CLEANUP:
    return res;
}

static mp_err point_mul_two_${curve.name}_wrap(
    const mp_int *n1, const mp_int *n2,
    const mp_int *in_x, const mp_int *in_y,
    mp_int *out_x, mp_int *out_y,
    const ECGroup *group)
{
    unsigned char b_x[${curve.fe_nbytes}];
    unsigned char b_y[${curve.fe_nbytes}];
    unsigned char b_n1[${curve.fe_nbytes}];
    unsigned char b_n2[${curve.fe_nbytes}];
    mp_err res;

    /* If n2 == NULL or 0, this is just a base-point multiplication. */
    if (n2 == NULL || mp_cmp_z(n2) == MP_EQ)
        return point_mul_g_${curve.name}_wrap(n1, out_x, out_y, group);

    /* If n1 == NULL or 0, this is just an arbitary-point multiplication. */
    if (n1 == NULL || mp_cmp_z(n1) == MP_EQ)
        return point_mul_${curve.name}_wrap(n2, in_x, in_y, out_x, out_y, group);

    ARGCHK(in_x != NULL && in_y != NULL && out_x != NULL
           && out_y != NULL, MP_BADARG);

   /* fail on out of range scalars */
   if (mpl_significant_bits(n1) > ${curve.p.bit_length()}
       || mp_cmp_z(n1) != MP_GT
       || mpl_significant_bits(n2) > ${curve.p.bit_length()}
       || mp_cmp_z(n2) != MP_GT)
       return MP_RANGE;

    MP_CHECKOK(mp_to_fixlen_octets(n1, b_n1, ${curve.fe_nbytes}));
    MP_CHECKOK(mp_to_fixlen_octets(n2, b_n2, ${curve.fe_nbytes}));
    MP_CHECKOK(mp_to_fixlen_octets(in_x, b_x, ${curve.fe_nbytes}));
    MP_CHECKOK(mp_to_fixlen_octets(in_y, b_y, ${curve.fe_nbytes}));
    MP_BE2LE(b_x);
    MP_BE2LE(b_y);
    MP_BE2LE(b_n1);
    MP_BE2LE(b_n2);
    ${curve.point_mul_two}(b_x, b_y, b_n1, b_n2, b_x, b_y);
    MP_BE2LE(b_x);
    MP_BE2LE(b_y);
    MP_CHECKOK(mp_read_unsigned_octets(out_x, b_x, ${curve.fe_nbytes}));
    MP_CHECKOK(mp_read_unsigned_octets(out_y, b_y, ${curve.fe_nbytes}));

CLEANUP:
    return res;
}

## TODO BBB below is a hack since we cannot know NSS internal names
mp_err ec_group_set_${curve.name}(ECGroup *group, ECCurveName name)
{
    if (name == ECCurve_NIST_P${curve.p.bit_length()}) {
        group->base_point_mul = &point_mul_g_${curve.name}_wrap;
        group->point_mul = &point_mul_${curve.name}_wrap;
        group->points_mul = &point_mul_two_${curve.name}_wrap;
    }
    return MP_OKAY;
}
#endif

#if defined(OPENSSL_BUILDING_OPENSSL) || defined(RIG_GOST)
#include <openssl/ec.h>

/* the zero field element */
static const unsigned char const_zb[${curve.fe_nbytes}] = {0};

/*-
 * An OpenSSL wrapper for simultaneous scalar multiplication.
 * r := n * G + m * q
 */
#  ifdef OPENSSL_BUILDING_OPENSSL
static
#  endif
int point_mul_two_${curve.name}_wrap(const EC_GROUP *group, EC_POINT *r, const BIGNUM *n, const EC_POINT *q, const BIGNUM *m, BN_CTX *ctx) {
    int ret = 0;
    unsigned char b_x[${curve.fe_nbytes}];
    unsigned char b_y[${curve.fe_nbytes}];
    unsigned char b_n[${curve.fe_nbytes}];
    unsigned char b_m[${curve.fe_nbytes}];
    BIGNUM *x = NULL, *y = NULL;

    BN_CTX_start(ctx);
    x = BN_CTX_get(ctx);
    if ((y = BN_CTX_get(ctx)) == NULL
        /* pull out coords as bytes */
        || !EC_POINT_get_affine_coordinates(group, q, x, y, ctx)
        || BN_bn2lebinpad(x, b_x, ${curve.fe_nbytes}) != ${curve.fe_nbytes}
        || BN_bn2lebinpad(y, b_y, ${curve.fe_nbytes}) != ${curve.fe_nbytes}
        || BN_bn2lebinpad(n, b_n, ${curve.fe_nbytes}) != ${curve.fe_nbytes}
        || BN_bn2lebinpad(m, b_m, ${curve.fe_nbytes}) != ${curve.fe_nbytes})
        goto err;
    /* do the simultaneous scalar multiplication */
    ${curve.point_mul_two}(b_x, b_y, b_n, b_m, b_x, b_y);
    /* check for infinity */
    if (CRYPTO_memcmp(const_zb, b_x, ${curve.fe_nbytes}) == 0
        && CRYPTO_memcmp(const_zb, b_y, ${curve.fe_nbytes}) == 0) {
        if (!EC_POINT_set_to_infinity(group, r))
            goto err;
    } else {
        /* otherwise, pack the bytes into the result */
        if (BN_lebin2bn(b_x, ${curve.fe_nbytes}, x) == NULL
            || BN_lebin2bn(b_y, ${curve.fe_nbytes}, y) == NULL
            || !EC_POINT_set_affine_coordinates(group, r, x, y, ctx))
            goto err;
    }
    ret = 1;
err:
    BN_CTX_end(ctx);
    return ret;
}

/*-
 * An OpenSSL wrapper for variable point scalar multiplication.
 * r := m * q
 */
#  ifdef OPENSSL_BUILDING_OPENSSL
static
#  endif
int point_mul_${curve.name}_wrap(const EC_GROUP *group, EC_POINT *r, const EC_POINT *q, const BIGNUM *m, BN_CTX *ctx) {
    int ret = 0;
    unsigned char b_x[${curve.fe_nbytes}];
    unsigned char b_y[${curve.fe_nbytes}];
    unsigned char b_m[${curve.fe_nbytes}];
    BIGNUM *x = NULL, *y = NULL;

    BN_CTX_start(ctx);
    x = BN_CTX_get(ctx);
    if ((y = BN_CTX_get(ctx)) == NULL
        /* pull out coords as bytes */
        || !EC_POINT_get_affine_coordinates(group, q, x, y, ctx)
        || BN_bn2lebinpad(x, b_x, ${curve.fe_nbytes}) != ${curve.fe_nbytes}
        || BN_bn2lebinpad(y, b_y, ${curve.fe_nbytes}) != ${curve.fe_nbytes}
        || BN_bn2lebinpad(m, b_m, ${curve.fe_nbytes}) != ${curve.fe_nbytes})
        goto err;
    /* do the variable scalar multiplication */
    ${curve.point_mul}(b_x, b_y, b_m, b_x, b_y);
    /* check for infinity */
    if (CRYPTO_memcmp(const_zb, b_x, ${curve.fe_nbytes}) == 0
        && CRYPTO_memcmp(const_zb, b_y, ${curve.fe_nbytes}) == 0) {
        if (!EC_POINT_set_to_infinity(group, r))
            goto err;
    } else {
        /* otherwise, pack the bytes into the result */
        if (BN_lebin2bn(b_x, ${curve.fe_nbytes}, x) == NULL
            || BN_lebin2bn(b_y, ${curve.fe_nbytes}, y) == NULL
            || !EC_POINT_set_affine_coordinates(group, r, x, y, ctx))
            goto err;
    }
    ret = 1;
err:
    BN_CTX_end(ctx);
    return ret;
}

/*-
 * An OpenSSL wrapper for fixed scalar multiplication.
 * r := n * G
 */
#  ifdef OPENSSL_BUILDING_OPENSSL
static
#  endif
int point_mul_g_${curve.name}_wrap(const EC_GROUP *group, EC_POINT *r, const BIGNUM *n, BN_CTX *ctx) {
    int ret = 0;
    unsigned char b_x[${curve.fe_nbytes}];
    unsigned char b_y[${curve.fe_nbytes}];
    unsigned char b_n[${curve.fe_nbytes}];
    BIGNUM *x = NULL, *y = NULL;

    BN_CTX_start(ctx);
    x = BN_CTX_get(ctx);
    if ((y = BN_CTX_get(ctx)) == NULL
        || BN_bn2lebinpad(n, b_n, ${curve.fe_nbytes}) != ${curve.fe_nbytes})
        goto err;
    /* do the fixed scalar multiplication */
    ${curve.point_mul_g}(b_x, b_y, b_n);
    /* check for infinity */
    if (CRYPTO_memcmp(const_zb, b_x, ${curve.fe_nbytes}) == 0
        && CRYPTO_memcmp(const_zb, b_y, ${curve.fe_nbytes}) == 0) {
        if (!EC_POINT_set_to_infinity(group, r))
            goto err;
    } else {
        /* otherwise, pack the bytes into the result */
        if (BN_lebin2bn(b_x, ${curve.fe_nbytes}, x) == NULL
            || BN_lebin2bn(b_y, ${curve.fe_nbytes}, y) == NULL
            || !EC_POINT_set_affine_coordinates(group, r, x, y, ctx))
            goto err;
    }
    ret = 1;
err:
    BN_CTX_end(ctx);
    return ret;
}

# ifdef OPENSSL_BUILDING_OPENSSL
#include "ec_local.h"

/* byte encoding of the standard base point */
static const unsigned char const_g[${2 * curve.fe_nbytes + 1}] = {
    /* uncompressed */
    0x04,
    /* x(G) */\
    ${fe_tobytes(curve.gx, curve.fe_nbytes)},
    /* y(G) */\
    ${fe_tobytes(curve.gy, curve.fe_nbytes)}
};

/*-
 * Implements the "mul" function pointer inside OpenSSL's EC_METHOD.
 * Relevant cases:
 * r := scalar * G (calls fixed scmul wrapper)
 * r := scalars[0] * points[0] (calls variable scmul wrapper)
 * r := scalar * G + scalars[0] * points[0] (calls simultaneous scmul wrapper)
 * Anything else, it reverts to the default and also when:
 * - using a custom generator
 * - using out of range scalars
 */
static int ec_GFp_mul_${curve.name}(const EC_GROUP *group, EC_POINT *r, const BIGNUM *scalar,
                    size_t num, const EC_POINT **points, const BIGNUM **scalars,
                    BN_CTX *ctx) {
    if (group == NULL || r == NULL || ctx == NULL)
        return ec_wNAF_mul(group, r, scalar, num, points, scalars, ctx);

    /* check for custom generator */
    if (scalar != NULL) {
        unsigned char buf_g[${2 * curve.fe_nbytes + 1}];

        if (EC_POINT_point2oct(group, EC_GROUP_get0_generator(group), POINT_CONVERSION_UNCOMPRESSED, buf_g, ${2 * curve.fe_nbytes + 1}, ctx) != ${2 * curve.fe_nbytes + 1})
            return 0;

        /* on generator mismatch, use the default */
        if (memcmp(const_g, buf_g, ${2 * curve.fe_nbytes + 1}) != 0)
            return ec_wNAF_mul(group, r, scalar, num, points, scalars, ctx);
    }

    if (scalar != NULL && num == 1 && scalars != NULL && scalars[0] != NULL && points != NULL && points[0] != NULL) {
        /* verification */
        if (BN_num_bits(scalar) > EC_GROUP_order_bits(group)
            || BN_is_negative(scalar)
            || BN_num_bits(scalars[0]) > EC_GROUP_order_bits(group)
            || BN_is_negative(scalars[0]))
            return ec_wNAF_mul(group, r, scalar, num, points, scalars, ctx);
        return point_mul_two_${curve.name}_wrap(group, r, scalar, points[0], scalars[0], ctx);
    } else if (scalar != NULL && num == 0) {
        /* mul g */
        if (BN_num_bits(scalar) > EC_GROUP_order_bits(group)
            || BN_is_negative(scalar))
            return ec_wNAF_mul(group, r, scalar, num, points, scalars, ctx);
        return point_mul_g_${curve.name}_wrap(group, r, scalar, ctx);
    } else if (scalar == NULL && num == 1 && scalars[0] != NULL && points != NULL && points[0] != NULL) {
        /* mul */
        if (BN_num_bits(scalars[0]) > EC_GROUP_order_bits(group)
            || BN_is_negative(scalars[0]))
            return ec_wNAF_mul(group, r, scalar, num, points, scalars, ctx);
        return point_mul_${curve.name}_wrap(group, r, points[0], scalars[0], ctx);
    }
    return ec_wNAF_mul(group, r, scalar, num, points, scalars, ctx);
}

const EC_METHOD *ec_GFp_${curve.name}_method(void)
{
    static const EC_METHOD ret = {
        EC_FLAGS_DEFAULT_OCT,
        NID_X9_62_prime_field,
        ec_GFp_simple_group_init,
        ec_GFp_simple_group_finish,
        ec_GFp_simple_group_clear_finish,
        ec_GFp_simple_group_copy,
        ec_GFp_simple_group_set_curve,
        ec_GFp_simple_group_get_curve,
        ec_GFp_simple_group_get_degree,
        ec_group_simple_order_bits,
        ec_GFp_simple_group_check_discriminant,
        ec_GFp_simple_point_init,
        ec_GFp_simple_point_finish,
        ec_GFp_simple_point_clear_finish,
        ec_GFp_simple_point_copy,
        ec_GFp_simple_point_set_to_infinity,
        ec_GFp_simple_point_set_affine_coordinates,
        ec_GFp_simple_point_get_affine_coordinates,
        0, /* point_set_compressed_coordinates */
        0, /* point2oct */
        0, /* oct2point */
        ec_GFp_simple_add,
        ec_GFp_simple_dbl,
        ec_GFp_simple_invert,
        ec_GFp_simple_is_at_infinity,
        ec_GFp_simple_is_on_curve,
        ec_GFp_simple_cmp,
        ec_GFp_simple_make_affine,
        ec_GFp_simple_points_make_affine,
        ec_GFp_mul_${curve.name}, /* mul */
        0, /* precompute_mult */
        0, /* have_precompute_mult */
        ec_GFp_simple_field_mul,
        ec_GFp_simple_field_sqr,
        0, /* field_div */
        ec_GFp_simple_field_inv,
        0, /* field_encode */
        0, /* field_decode */
        0, /* field_set_to_one */
        ec_key_simple_priv2oct,
        ec_key_simple_oct2priv,
        0, /* set_private */
        ec_key_simple_generate_key,
        ec_key_simple_check_key,
        ec_key_simple_generate_public_key,
        0, /* keycopy */
        0, /* keyfinish */
        ecdh_simple_compute_key,
        ecdsa_simple_sign_setup,
        ecdsa_simple_sign_sig,
        ecdsa_simple_verify_sig,
        0, /* field_inverse_mod_ord */
        ec_GFp_simple_blind_coordinates,
        ec_GFp_simple_ladder_pre,
        ec_GFp_simple_ladder_step,
        ec_GFp_simple_ladder_post
    };

    return &ret;
}

#  ifdef KIILA_OPENSSL_EMIT_CURVEDEF
% if curve.oid:
/* ${curve.oid.replace('.', ' ')} : ${curve.name} */
% endif
static const struct {
    EC_CURVE_DATA h;
    unsigned char data[0 + ${curve.fe_nbytes} * 6];
} _EC_${curve.name} = {
    {
       NID_X9_62_prime_field, 0, ${curve.fe_nbytes}, ${curve.h}
    },
    {
        /* no seed */
        /* p */\
        ${fe_tobytes(curve.p, curve.fe_nbytes)},
        /* a */\
        ${fe_tobytes(curve.a, curve.fe_nbytes)},
        /* b */\
        ${fe_tobytes(curve.b, curve.fe_nbytes)},
        /* x */\
        ${fe_tobytes(curve.gx, curve.fe_nbytes)},
        /* y */\
        ${fe_tobytes(curve.gy, curve.fe_nbytes)},
        /* order */\
        ${fe_tobytes(curve.q, curve.fe_nbytes)}
    }
};
#  endif

# endif
#endif

<%def name="fe_tobytes(x, pad)">
<%
bytes = []
while x:
    bytes.append(x & 0xFF)
    x >>= 8
while len(bytes) < pad:
    bytes.append(0)
bytes = map(lambda b: '0x%02X' % b, bytes[::-1])
bytes = ','.join(bytes)
%>${bytes}\
</%def>
