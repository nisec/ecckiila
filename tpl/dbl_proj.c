<%namespace file="fe_inv.c" import="vomit_op3"/>
/*-
 * Q := 2P, both projective, Q and P same pointers OK
% for comment in comments:
 * ${comment}
% endfor
 */
static void point_double(pt_prj_t *Q, const pt_prj_t *P) {
    /* temporary variables */
    fe_t ${', '.join([t for t in sorted(uniques) if t.startswith('t')])};
    /* constants */
% for i in set(input) & {'e', 'a', 'b', 'b3'}:
    const limb_t *${i} = const_${i};
% endfor
% if curve.is_edwards():
    /* set pointers for Edwards curve arith */
    const limb_t *${input[0]} = P->X;
    const limb_t *${input[1]} = P->Y;
    const limb_t *${input[3]} = P->Z;
    limb_t *${output[0]} = Q->X;
    limb_t *${output[1]} = Q->Y;
    limb_t *${output[2]} = Q->T;
    limb_t *${output[3]} = Q->Z;
% else:
    /* set pointers for legacy curve arith */
    const limb_t *${input[0]} = P->X;
    const limb_t *${input[1]} = P->Y;
    const limb_t *${input[2]} = P->Z;
    limb_t *${output[0]} = Q->X;
    limb_t *${output[1]} = Q->Y;
    limb_t *${output[2]} = Q->Z;
% endif

    /* the curve arith formula */\
    ${vomit_op3(lines)}
}
