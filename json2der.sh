#!/bin/bash

for curve in secp128r2 secp192r1 brainpoolP192t1 brainpoolP192r1 Wei25519 secp256r1 secp256k1 id_GostR3410_2001_TestParamSet id_GostR3410_2001_CryptoPro_A_ParamSet id_GostR3410_2001_CryptoPro_B_ParamSet id_GostR3410_2001_CryptoPro_C_ParamSet id_tc26_gost_3410_2012_256_paramSetA SM2 brainpoolP256t1 MDCurve201601 brainpoolP320t1 secp384r1 brainpoolP384t1 Wei448 id_tc26_gost_3410_2012_512_paramSetA id_tc26_gost_3410_2012_512_paramSetB id_tc26_gost_3410_2012_512_paramSetC brainpoolP512t1 secp521r1; do
    JSON=ecp/$curve/curve.json

    if grep -q '"stress":' $JSON; then continue; fi

    curve_p=$(sed -rn 's/.*"p": "0x([0-9A-F]+)".*/\U\1/p' $JSON)
    curve_a=$(sed -rn 's/.*"a": "0x([0-9A-F]+)".*/\U\1/p' $JSON)
    curve_b=$(sed -rn 's/.*"b": "0x([0-9A-F]+)".*/\U\1/p' $JSON)
    curve_n=$(sed -rn 's/.*"n": "0x([0-9A-F]+)".*/\U\1/p' $JSON)
    curve_h=$(sed -rn 's/.*"h": "0x([0-9A-F]+)".*/\U\1/p' $JSON)
    curve_g=$(sed -rn 's/.*"g": "0x([0-9A-F]+)".*/\U\1/p' $JSON)

    cat <<EOF > $curve.cfg
asn1 = SEQUENCE:SpecifiedECDomain_$curve

[SpecifiedECDomain_$curve]
version = INTEGER:1
fieldID = SEQUENCE:fieldID_$curve
curve = SEQUENCE:Curve_$curve
base = FORMAT:HEX,OCTETSTRING:$curve_g
order = INTEGER:0x$curve_n
cofactor = INTEGER:0x$curve_h

[fieldID_$curve]
id-fieldType = OBJECT:prime-field
Prime-P = INTEGER:0x$curve_p

[Curve_$curve]
a = FORMAT:HEX,OCTETSTRING:$curve_a
b = FORMAT:HEX,OCTETSTRING:$curve_b
EOF

    openssl asn1parse -genconf $curve.cfg -out $curve.der -noout
    ./ecstresstest $curve.der > $curve.json
    jq -s '.[0] * .[1]' $JSON $curve.json > $curve.tmp.json
    mv $curve.tmp.json $JSON
done
