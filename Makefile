OPENSSL_ROOT_DIR=$(HOME)/.local/ssl

CURVES=secp128r2 secp192r1 brainpoolP192t1 brainpoolP192r1 Wei25519 secp256r1 secp256k1 id_GostR3410_2001_TestParamSet id_GostR3410_2001_CryptoPro_A_ParamSet id_GostR3410_2001_CryptoPro_B_ParamSet id_GostR3410_2001_CryptoPro_C_ParamSet id_tc26_gost_3410_2012_256_paramSetA SM2 brainpoolP256t1 MDCurve201601 brainpoolP320t1 secp384r1 brainpoolP384t1 Wei448 id_tc26_gost_3410_2012_512_paramSetA id_tc26_gost_3410_2012_512_paramSetB id_tc26_gost_3410_2012_512_paramSetC brainpoolP512t1 secp521r1

$(CURVES):
	mkdir -p $(CURDIR)/ecp/$@/build/
	cd $(CURDIR)/ecp/$@/build/ && \
	($(MAKE) -s clean || true) && \
	rm -fr * && \
	cmake .. && \
	$(MAKE) -s check && \
	$(MAKE) -s clean

ecstresstest: ecstresstest.c
	$(CC) -o $@ $< -lcrypto -I$(OPENSSL_ROOT_DIR)/include -L$(OPENSSL_ROOT_DIR)/lib -Wl,-rpath=$(OPENSSL_ROOT_DIR)/lib

test: $(CURVES)

clean:
	rm -f ecstresstest *.cfg *.der *.json

.PHONY: clean test $(CURVES)
