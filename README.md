## Introduction

**ECCKiila** allows to dynamically create portable C-code (supporting both
64-bit and 32-bit architectures, no alignment or endianness assumptions)
underlying Elliptic Curve Cryptography (ECC) cryptosystems. Our generated ECC
layer as well as the generated rigging allow seamless integration into
downstream projects (such as
[OpenSSL](https://github.com/openssl/openssl/),
[gost-engine](https://github.com/gost-engine/engine),
[NSS](https://hg.mozilla.org/projects/nss/),
etc.),
all driven by Python [Mako](https://www.makotemplates.org/) templating. This
figure summarizes our proposed full stack implementation named **ECCKiila**.
[Preprint with all the details](https://arxiv.org/abs/2007.11481) and benchmarks is now available.

```mermaid
graph TD
st("Public parameters: {p, a, b, g, q, h} (and possibly also {e, d, u, v})")
fiat["generate GF(p) arithmetic using fiat-crypto"]
condh{h == 1?}

subgraph " "
wei["Set Weierstrass model"]
condw1{a == -3?}
wei1["generate curve arithmetic with a=-3"]
condw2{a == 0?}
wei2["generate curve arithmetic with a=0"]
end

subgraph " "
ted["Set Twisted Edwards model"]
conde1{e == 1?}
ted1["generate curve arithmetic with e=1"]
conde2{e == -1?}
ted2["generate curve arithmetic with e=-1"]
end

gen["generate generic curve arithmetic"]
point["generate fixed, variable, and double point multiplication procedures"]

subgraph " "
standalone[/"standalone C-code"/]
nss[/"NSS C-code"/]
openssl[/"OpenSSL C-code"/]
gost[/"Gost-engine C-code"/]
end
st --> fiat --> condh
condh -- yes --> wei
wei --> condw1 -- yes --> wei1 --> point
condw1 -- no --> condw2 -- yes--> wei2 --> point
condw2 -- no--> gen
condh -- no --> ted

ted --> conde1 -- no --> conde2 -- no --> gen --> point
conde2 -- yes --> ted2 --> point
conde1 -- yes --> ted1 --> point

point --> standalone
point --> nss
point --> openssl
point --> gost
```

## Features

- 100% computer generated, including the downstream rigging
- Weierstrass and (Twisted) Edwards curve models
- Most of the Galois Field (GF) layer comes from
  [fiat-crypto project](https://github.com/mit-plv/fiat-crypto),
  so you get some formal verification guarantees from there (which is where the
  guarantees end)
- Complete EC arithmetic, generated from [EFD](https://www.hyperelliptic.org/EFD/) `op3` format.
- Constant Time scalar point multiplication by fixed point (**keygen** and **sign** with fixed base point `g`)
- Constant Time scalar point multiplication by variable point (**derive** with variable point `P = [a]g`)
- Variable Time scalar point multiplication for two points `[b]g + [c]P` (**verify**)
- Both 64-bit and 32-bit support
- No alignment assumptions (not explicitly tested)
- No endianness assumptions (coded that way, yet untested on BE)

## Project structure

* `ecp/*`: curve parameters and fiat-crypto arithmetic
* `op3/*`: EC arithmetic pseudocode
* `tpl/*`: Mako templates
* `inv2op3.py`: generates GF inversion code
* `main.py`: Mako templating logic
* `test_gen.py`: unit test generator

## Prerequisites

```
sudo apt install unifdef
sudo apt install cmake
sudo apt install python3-mako
sudo apt install libgmp-dev
sudo apt install jq
sudo -H pip3 install fastecdsa
```

Or if you prefer local install:

```
pip3 install --user fastecdsa
```

### clang

While the end C output of ECCKiila can be compiled with whatever, the internal
tooling uses `clang` and `clang-format`. We suggest `clang` >= 10 from
[PPA](https://apt.llvm.org/).

ECCKiila tests both 64-bit and 32-bit builds so you will likely need
distro-specific stuff for that.

```
sudo apt install gcc-multilib
```

### sharness

ECCKiila uses [sharness](https://github.com/chriscool/sharness) for
[TAP](https://en.wikipedia.org/wiki/Test_Anything_Protocol) testing.
Ubuntu >= 19 has the `sharness` package:

```
sudo apt install sharness
```

But for Ubuntu < 19:

```
cd /usr/local/src
git clone https://github.com/chriscool/sharness.git
cd sharness/
sudo checkinstall --strip=no --stripso=no --pkgname=sharness-debug --provides=sharness-debug --default make install prefix=/usr/local
```

### fiat-crypto

ECCKiila uses [fiat-crypto](https://github.com/mit-plv/fiat-crypto) for most of
the field arithmetic.

* If you plan on adding curves or changing the `fiat-crypto` source for a curve,
  then you need to follow these steps.
* Otherwise, you can skip these steps.

First you need `coq`. This should work for you on Ubuntu 19+:

```
sudo apt install libcoq-ocaml-dev
```

Otherwise, find a PPA, install from source, or figure it out.
`libcoq` >= 8.9.1 should be sufficient.

Then clone the `fiat-crypto` repo and build it:

```
git clone --recursive https://github.com/mit-plv/fiat-crypto.git
cd fiat-crypto/
make -j32 SKIP_BEDROCK2=1 standalone-ocaml
```

Check the [fiat-crypto](https://github.com/mit-plv/fiat-crypto) documentation
in `README.md` for what those build options do.

After building, to optionally install system-wide:

```
sudo install --mode=755 src/ExtractionOCaml/word_by_word_montgomery /usr/local/bin
sudo install --mode=755 src/ExtractionOCaml/saturated_solinas /usr/local/bin
sudo install --mode=755 src/ExtractionOCaml/unsaturated_solinas /usr/local/bin
```

### Valgrind (optional)

Install `valgrind`:

```
sudo apt install valgrind
```

## Building, testing, and benchmarking: single curve

For a debug build:

```
cd ecp/secp256k1/
mkdir build
cd build/
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```

For a release / benchmarking build:

```
cd ecp/secp256k1/
mkdir build
cd build/
cmake ..
make check
make bench
```

Replace `secp256k1` with whatever curve you want.
CMake can be fickle so if you are frequently switching between Debug / Release
build, wipe everything:

```
cd build/
make clean
rm -fr *
cmake ..
```

For a `valgrind` build (optional):

```
cd ecp/secp256k1/
mkdir build
cd build/
cmake -DECCKIILA_VAL=ON ..
make validation
```

## Building and testing: all curves

Check `.gitlab-ci.yml` to see how our CI tests 64 and 32-bit builds of all
curves in the `ecp/` directory. TL;DR from the root directory:

```
make test
CFLAGS="-m32" make test
```

## Adding a curve

In general, the process goes like

1. Add a subdirectory under `ecp` for the curve.
2. Put a JSON file there with curve parameters, and link to the build system.
3. Add 64 and 32-bit `fiat-crypto` code.

### Restrictions

1. One of `h == 1` or `h mod 4 == 0` must hold.
2. Curves with `b == 0` are not supported.

### Example: numsp256d1

Microsoft has some
[Nothing Up My Sleeve](https://www.microsoft.com/en-us/research/publication/specification-of-curve-selection-and-supported-curve-parameters-in-msr-ecclib/)
curves: let's take `numsp256d1`.

```
mkdir ecp/numsp256d1
cd ecp/numsp256d1/
ln -s ../@@ecp@@_CMakeLists.txt CMakeLists.txt
```

Now add the parameters from the above linked PDF (Section 5).

```
$ cat curve.json
{
  "name": "numsp256d1",
  "p": "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF43",
  "a": "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40",
  "b": "0x025581",
  "g": "0x04BC9ED6B65AAADB61297A95A04F42CB0983579B0903D4C73ABC52EE1EB21AACB1D08FC0F13399B6A673448BF77E04E035C955C3D115310FBB80B5B9CB2184DE9F",
  "n": "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE43C8275EA265C6020AB20294751A825",
  "h": "0x01"
}
```

These parameters should be self-explanatory, except for base point `g` which has
the following syntax for historical reasons.

```
"g": "0x04(x_coordinate_hex)(y_coordinate_hex)"
```

Now add the 64 and 32-bit `fiat-crypto` code.

```
word_by_word_montgomery --static --use-value-barrier numsp256d1 64 '2^256 - 189' > fiat_64.c
word_by_word_montgomery --static --use-value-barrier numsp256d1 32 '2^256 - 189' > fiat_32.c
```

Depending on the prime form, you want either the `word_by_word_montgomery` or
`unsaturated_solinas` strategy (the latter would've been more appropriate here
performance wise, but this is just an example). If you are getting strange
`fiat-crypto` failures, try increasing stack space:

```
Fatal error: exception Stack overflow
$ ulimit -S -s 1000000
```

Then just build and test:

```
mkdir build
cd build/
cmake ..
make check
```

If you want this curve to be tested with all the other curves (e.g. for sending
an MR), you should add it to the `CURVES` variable in the root `Makefile`.

If you see a warning like this in the generated `fe_inv.op3` file:

```
# SRC: sliding window w=5 (TODO FIXME unoptimized)
```

Then depending on the form of your curve prime `p`, you might want to optimize
the addition sequence for FLT inversion. Unfortunately this is currently
undocumented in `inv2op3.py`, but the dictionary `d` contains such sequences
that are curve-specific, and has some unfriendly tooling to generate the
corresponding string for special `p`.

Adding `h != 1` (yet `h = 0 mod 4`) curves is currently undocumented, but in
general you need four additional JSON parameters: `u` and `v`
[described here](https://tools.ietf.org/html/rfc7836#section-5.2) that are the
Edwards curve equivalent of `g`, as well as the birationally equivalent Edwards
curve coefficients `e` and `d`. Although that specific mapping is not a
pre-requisite: see `Wei25519` and `Wei448` for exceptions. If you have such an
exception, you need to add the corresponding logic in the curve-specific
constructor of `main.py`.

## Downstream projects: stripping the source for rigging

ECCKiila generates tests, functions, and rigging for downstream projects.
Depending on your use case for the output C, you probably want a subset of that.
Use [unifdef](https://dotat.at/prog/unifdef/) to select what you need.

For example, to get the C with `gost-engine` rigging:

```
unifdef ../ecp_id_tc26_gost_3410_2012_256_paramSetA.c -URIG_NULL -URIG_NSS -DRIG_GOST -UOPENSSL_BUILDING_OPENSSL -UKIILA_OPENSSL_EMIT_CURVEDEF -ULIB_TEST > /path/to/gost-engine/ecp_id_tc26_gost_3410_2012_256_paramSetA.c
```

For `openssl` rigging:

```
unifdef ../ecp_secp384r1.c -URIG_NSS -URIG_GOST -URIG_NULL -DOPENSSL_BUILDING_OPENSSL -UKIILA_OPENSSL_EMIT_CURVEDEF -ULIB_TEST > /path/to/openssl/crypto/ec/ecp_secp384r1.c
```

For `libnss` rigging:

```
unifdef ../ecp_secp384r1.c -DRIG_NSS -URIG_GOST -URIG_NULL -UOPENSSL_BUILDING_OPENSSL -UKIILA_OPENSSL_EMIT_CURVEDEF -ULIB_TEST > /path/to/nss/lib/freebl/ecl/ecp_secp384r1.c
```

Handy oneliners:

```
# all GOST curves
for d in id_*; do unifdef $d/ecp_$d.c -URIG_NSS -DRIG_GOST -URIG_NULL -UOPENSSL_BUILDING_OPENSSL -UKIILA_OPENSSL_EMIT_CURVEDEF -ULIB_TEST > ~/svnrepos/gost-engine/ecp_$d.c; done
# select curves for OpenSSL
export CURVES="secp192r1 brainpoolP192t1 Wei25519 secp256r1 secp256k1 SM2 brainpoolP256t1 MDCurve201601 brainpoolP320t1 secp384r1 brainpoolP384t1 Wei448 brainpoolP512t1 secp521r1"
for c in $CURVES; do unifdef ecp/$c/ecp_$c.c -URIG_NSS -URIG_GOST -URIG_NULL -DOPENSSL_BUILDING_OPENSSL -ULIB_TEST > /path/to/openssl/crypto/ec/ecp_$c.c; done
# all legacy curves for NSS
for c in secp256r1 secp384r1 secp521r1; do unifdef ecp/$c/ecp_$c.c -DRIG_NSS -URIG_GOST -URIG_NULL -UOPENSSL_BUILDING_OPENSSL -UKIILA_OPENSSL_EMIT_CURVEDEF -ULIB_TEST > /path/to/nss/lib/freebl/ecl/ecp_$c.c; done
```

## Credits

### Authors

* Luis Rivera-Zamarripa (Tampere University, Tampere, Finland)
* Jesús-Javier Chi-Domínguez (Tampere University, Tampere, Finland)
* Billy Bob Brumley (Tampere University, Tampere, Finland)

### Funding

This project has received funding from the European Research Council (ERC) under
the European Union's Horizon 2020 research and innovation programme (grant
agreement No 804476).
