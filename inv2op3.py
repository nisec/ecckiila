#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# sed 's/\(.*\)=\(.*\)\*\(.*\)/\1=MUL\(\2,\3\)/' foo.txt > foo.py

import sys
import json
import bisect

try:
    name = sys.argv[1]
    with open('ecp/%s/curve.json' % name) as fp:
        curve = json.load(fp)
        assert(curve['name'] == name)
except:
    print('usage: python3 %s <curve_name>' % (sys.argv[0]))
    sys.exit(1)

p = int(curve['p'], 16)
exp = p - 2

windows = filter(lambda x: '1' in x, format(exp, 'b').split('0'))
lens = list(map(lambda x: len(x), windows))[::-1]

BEST = None
best_seq = None

# total hack
def addseq(target, seq):
    global BEST, best_seq
    if seq[-1] > target[-1]: return False
    if len(seq) == BEST:
        for num in target:
            i = bisect.bisect_left(seq, num)
            if i == len(seq) or seq[i] != num: return False
        print('done', target, seq)
        best_seq = seq
        return True
    #print(target), print(seq)
    for num in seq[::-1]:
        newseq = seq + [seq[-1] + num]
        if addseq(target, newseq): return True
    return False

def get_seq(lens):
    global BEST, best_seq
    for i in range(lens[-1].bit_length(), 4*lens[-1].bit_length()):
        BEST=i
        addseq(lens, [1])
        if best_seq: break
    return best_seq

def sliding_window(e, W=5, warn=False):
    windows = []
    while e:
        if not (e & 1):
            windows.append(0)
            e >>= 1
            continue
        windows.append(e % (1 << W))
        e >>= W

    print('# IN: ["t1"]')
    print('# OUT: ["output"]')
    if warn: print('# SRC: sliding window w=%d (TODO FIXME unoptimized)' % W)
    else: print('# SRC: sliding window w=%d' % W)
    print('acc=t1*t1')
    m = max(windows)
    prev = 't1'
    for i in range(3, m + 1, 2):
        if i in windows:
            print('t%d=%s*acc' % (i, prev))
            prev = 't%d' % i
        else:
            print('t%d=%s*acc' % (m, prev))
            prev = 't%d' % m
    d = windows.pop()
    print('acc=t%d*t%d' % (d, d))
    dostuff = False
    while windows:
        d = windows.pop()
        if d == 0:
            if dostuff: print('acc=acc*acc')
            dostuff = True
            continue
        for i in range(W):
            if dostuff: print('acc=acc*acc')
            dostuff = True
        if windows: print('acc=acc*t%d' % d)
        else: print('output=acc*t%d' % d)

d = {
'secp192r1': [1, 2, 4, 5, 10, 20, 40, 60, 62, 122, 127], # [1, 62, 127]
'Wei25519': [1, 2, 4, 8, 16, 32, 48, 50, 100, 200, 250], # [2, 1, 250]
'secp256r1': [1, 2, 4, 8, 10, 20, 30, 32], # [30, 32]
'secp256k1': [1, 2, 4, 8, 16, 20, 22, 44, 45, 89, 178, 223], # [1, 2, 1, 22, 223]
'SM2': [1, 2, 4, 8, 10, 20, 30, 31], # [31]
'secp384r1': [1, 2, 4, 8, 10, 20, 30, 32, 64, 84, 85, 170, 255], # [1, 30, 32, 255]
'Wei448': [1, 2, 4, 8, 16, 32, 64, 72, 74, 148, 222, 223], # [1, 222, 223]
'secp521r1': [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 516, 518, 519], # [1, 519]
'id_GostR3410_2001_CryptoPro_A_ParamSet' : [1, 2, 4, 8, 16, 32, 64, 80, 82, 164, 246], # [1, 1, 1, 2, 246]
'id_tc26_gost_3410_2012_256_paramSetA' : [1, 2, 4, 8, 16, 32, 64, 80, 82, 164, 246], # [1, 1, 1, 2, 246]
'id_tc26_gost_3410_2012_512_paramSetA' : [1, 2, 3, 6, 12, 24, 48, 96, 102, 198, 200, 400, 502], # [1, 1, 3, 502]
'id_tc26_gost_3410_2012_512_paramSetC' : [1, 2, 3, 6, 12, 24, 48, 96, 102, 198, 200, 400, 502] # [1, 1, 3, 502]
}

# if brainpool or otherwise random looking, or SW is good, use that
not_optimized = [
'id_GostR3410_2001_CryptoPro_B_ParamSet', # low weight so SW is pretty good
'id_GostR3410_2001_CryptoPro_C_ParamSet', # random looking
'id_tc26_gost_3410_2012_512_paramSetB', # low weight so SW is pretty good
'id_GostR3410_2001_TestParamSet', # low weight so SW is pretty good
'MDCurve201601', # random looking
'secp128r2' # regression testing only
]

if name not in d:
    if name.startswith('brainpool') or name in not_optimized: sliding_window(exp)
    else: sliding_window(exp, warn=True)
    sys.exit(0)

best_seq = d.get(name, None)
if not best_seq:
    best_seq = get_seq(lens)

print('# IN: ["t1"]')
print('# OUT: ["output"]')
print('# SRC: custom repunit addition chain')
print('# window part')
for i in range(1,len(best_seq)):
    sqrcnt = best_seq[i] - best_seq[i-1]
    print('acc=t%d*t%d' % (best_seq[i-1], best_seq[i-1]))
    for j in range(sqrcnt-1):
        print('acc=acc*acc')
    print('t%d=acc*t%d' % (best_seq[i], best_seq[i] - best_seq[i-1]))

print('# assembly part')
isone = True
var = None
binary = list(map(int, list(format(exp, 'b'))))
while binary:
    cnt = 0
    while binary and binary[0] == 1:
        binary.pop(0)
        cnt += 1
        if not isone: print('acc=acc*acc')
        if name == 'SM2' and cnt == 31: break
        if name == 'secp256r1' and cnt == 32: break
    if cnt > 0:
        if isone:
            isone = False
            var = 't%d' % cnt
        else:
            if binary: print('acc=acc*t%d' % cnt)
            else: print('output=acc*t%d' % cnt)
    cnt = 0
    while binary and binary[0] == 0:
        binary.pop(0)
        cnt += 1
        if var:
            print('acc=%s*%s' % (var,var))
            var = None
        else: print('acc=acc*acc')

